import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by omfg on 06.12.2016.
 */
public class MainWindow extends JFrame{
JScrollPane scrollPane;
JTable table;
JButton addBtn;
JButton removeBtn;
private PersonModel model;
    public MainWindow() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setSize(640,480);


        model= new PersonModel();
        scrollPane = new JScrollPane();
        getContentPane().setLayout(null);
        getContentPane().add(scrollPane);
        table =new JTable(model);

        addBtn = new JButton("Add");
        removeBtn = new JButton("Remove");

        scrollPane.setBounds(10,10,480,430);
        scrollPane.setViewportView(table);

        addBtn.setBounds(535,10,90,25);
        removeBtn.setBounds(535,45,90,25);
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Person person = new Person();
                person.setName(JOptionPane.showInputDialog("Enter name"));
                person.setSurname(JOptionPane.showInputDialog("Enter surname"));
                Main.persons.add(person);
                table.updateUI();

            }
        });
        removeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(table.getSelectedRow()==-1){return;}
                Main.persons.remove(table.getSelectedRow());
                table.updateUI();

            }
        });

        getContentPane().add(addBtn);
        getContentPane().add(removeBtn);









        setVisible(true);

    }

}
