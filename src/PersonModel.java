import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by omfg on 06.12.2016.
 */
public class PersonModel implements TableModel{
    private ArrayList<TableModelListener> listeners;

    @Override
    public int getRowCount() {
        return Main.persons.size();
    }

    public PersonModel() {
        listeners =new ArrayList<TableModelListener>();
    }

    @Override
    public int getColumnCount() {

        return 2;
    }

    @Override
    public String getColumnName(int index) {
        String return_string = "";
        switch (index){

            case 0:
                return_string= "Name";
                break;

            case 1:
                return_string= "Surname";
                break;
        }
return return_string;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int col ){
        return true;
    }

    @Override
    public Object getValueAt(int row, int col) {
        Object retrun_object = null;
        switch (col){
            case 0:
                retrun_object = Main.persons.get(row).getName();
                break;
            case 1:
                retrun_object = Main.persons.get(row).getSurname();
                break;

        }
        return retrun_object;

    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

switch (columnIndex){
    case 0:
        Main.persons.get(rowIndex).setName((String)aValue);
        break;
    case 1:
        Main.persons.get(rowIndex).setSurname((String)aValue);
        break;

}
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);

    }

    @Override
    public void removeTableModelListener(TableModelListener l) {


    }
}
