/**
 * Created by omfg on 06.12.2016.
 */
public class Person {
    private String name;
    private String surname;

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {

        return name;
    }

    public String getSurname() {
        return surname;
    }
}
